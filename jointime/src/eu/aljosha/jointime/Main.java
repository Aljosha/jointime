package eu.aljosha.jointime;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Aljosha on 06.06.2016.
 * http://aljosha.eu
 */

public class Main extends JavaPlugin implements Listener {

    @Override
    public void onEnable(){
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(this, this);
        saveDefaultConfig();

    }

    @EventHandler
    public void onJoin(PlayerLoginEvent e){
        Player p = e.getPlayer();
        if(!p.hasPermission("jointime.ignore")){
            SimpleDateFormat date = new SimpleDateFormat("HH");
            String time = date.format(new Date());
            System.out.println(time);
            if(!getConfig().getBoolean(time + "h")){
                e.disallow(PlayerLoginEvent.Result.KICK_FULL, ChatColor.translateAlternateColorCodes('&', getConfig().getString("kickmessage")));
            }

        }

    }
}
