JoinTime is a simple plugin for Craftbukkit to prevent players from joining the server at the wrong time.
It's written for Craftbukkit 1.8.8